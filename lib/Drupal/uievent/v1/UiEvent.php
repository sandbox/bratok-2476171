<?php
/**
 * @file 
 *  Библиотека обертка вокруг ctools_wizard_multistep_form для создания 
 *  пошаговых форм используя принцыпы ООП
 * @author 
 *  A. Bratko <bratko_@mail.ru>
 */

/**
 * @defgroup  <test_group> (group title)
 *  Описаниетестовой группы 
 * 
 */

namespace Drupal\uievent\v1;

class UiEvent{
  
  /**
   * Имя события 
   * @var type 
   */
  protected $name   = null;
  
  /**
   * Id объекта генерируещего событие 
   * @var type 
   */
  protected $target = null;
  /**
   * Дополнительные данные для обработчика события 
   * @var type 
   */
  protected $data   = null;
  static protected $vertion = 'v1';
  
  static function getInstance($name, $target, $data = null) {
    static::initJsLibrary();
    return new self($name, $target, $data);
  }
  
  static function initJsLibrary(){
    static $isCalled  = false;
    if (!$isCalled){
      $path = drupal_get_path('module', 'jqdialog');
      drupal_add_js( $path.'/js/'.static::$vertion.'/ui/uievent.js', 'file');
      $isCalled  = true;
    }  
  }
  
  public function __construct($name, $target, $data) {
    $this
      ->setName($name)
      ->setTarget($target)
      ->setData($data);
  }
  
  function setName($name){
    $this->name = $name;
    return $this;
  }
  function getName(){
    return $this->name;
  }
  function setTarget($target){
    $this->target = $target;
    return $this;
  }
  
  function getTarget(){
    return $this->target ;
  }
  
  function setData($data){
    $this->data = $data;
    return $this;
  }
  
  /**
   * Преобразует событие в команду запуска этого события на стороне браузера 
   * @param type $commandName
   * @return string
   */
  function toCommand($commandName){
    $command = array(
      'command' => 'UiEventCommand',
      'commandName' => $commandName, 
      'commandData' => array(
        'name'    => $this->getName(), 
        'target'  => $this->target, 
        'data'    => $this->data, 
      ),
    );
    return $command;
  }
  
  
}

 