var Drupal = Drupal || {};
(function($){
  
  var Controller = function(){
    /**
     * Включенные обработчики
     */
    this.on = {};
    /**
     * Выключенные обработчики, которые имеют дочерние обработчики. 
     * Если дочерних обработчиков нет, то обработчик просто удаляется из this.on
     */
    this.off = {};
    /**
     * Информация об обработчиках, которые ожидают прикрепления. 
     */
    this.awaiting = {};
    
  }
  
  /**
   * Обработчки команды поступающей с сервера 
   * @param {type} ajaxObj
   * @param {type} response
   * @param {type} status
   * @returns {undefined}
   */
  Controller.prototype.commandsHandler  = function(ajaxObj, response, status){
    if (Boolean(response.commandName) && Boolean(this[response.commandName])) {
      this[response.commandName](response);
    }
  }
  /**
   * Геренирует событие
   * @param {type} response
   * @returns {undefined}
   */
  Controller.prototype.trigger = function(response){
    var command = response.commandData;
    $('#' + command.target).trigger(command.name, command.data);
  }
  
  /**
   * Включает обработчик события   
   * @param EventHandler  handler
   * @returns {undefined}
   */
  Controller.prototype.onEventHandler = function(handler){
    var handlerId = handler.getId();
    if (Boolean(this.off[handlerId])){
      // есть старый обработчик стакимде именем, но использоаать его нельзя, так 
      var childHandlers = this.off[handlerId].getChildHandlers();
      handler.setChildHandlers(childHandlers);
      delete this.off[handlerId];
    }
    
    this.on[handlerId] = handler.on();
    if (Boolean(this.awaiting[handlerId])){
      delete this.awaiting[handlerId];
    }
    
    var parentHandlerId = handler.getParentHandlersId();
    if (parentHandlerId && Boolean(this.on[parentHandlerId])){
      var child = this.on[parentHandlerId].getChildHandlers();
      if (!$.inArray(child, handlerId)){
        child.push(handlerId);
      }
    }
    return this;
  }
  
  /**
   * Выключает обработчик события   
   * @param EventHandler  handler
   *  Выключаемый обработчик 
   * @param Boolean  toWaiting 
   *  Признак перевода обработчика в режим ожидания 
   * @returns {undefined}
   */
  Controller.prototype.offEventHandler = function(handler, toWaiting){
    var handlerId = handler.getId();
    handler.off();
    delete this.on[handlerId];
    if (Boolean(toWaiting)){
      this.awaiting[handlerId] = handler;
    }
    
    var childs = handler.getChildHandlers();
    if (childs.length){
      for(var key in childs){
        var childHandlerId = childs[key];
        if (this.on[childHandlerId]){
          this.offEventHandler(this.on[childHandlerId], true);
        }
      }
      this.off[handlerId] = handler.clean();
    }
    
    return this;
  }
  
  Controller.prototype.offEventHandlers = function(handlerOwner, eventHandlers){
    
    for (var eventName  in eventHandlers) {
      for (var handlerName in eventHandlers[eventName]){
        var handlerId = EventHandler.getId(handlerOwner, handlerName);
        if (Boolean(this.on[handlerId])){
          this.offEventHandler(this.on[handlerId]);
        }
      }
    }
  }
  
  Controller.prototype.onEventHandlers = function(handlerOwner, eventHandlers){
    for (var eventName  in eventHandlers) {
      for (var handlerName in eventHandlers[eventName]){
        var handlerData = eventHandlers[eventName][handlerName];
        var handler = new EventHandler(eventName, handlerName, handlerOwner, handlerData);
        var parentId = handler.getParentHandlersId(); 
        if (!Boolean(parentId) || Boolean(this.on[parentId])){
          this.onEventHandler(handler);
        }
        else {
          this.awaiting[handler.getId()] = handler;
        }
      }
    }
    
    // пытаемся подключить обработчики ожидающие подключения других обработчиков 
    for (var handlerId in this.awaiting){
      var parentHdId = this.awaiting[handlerId].getParentHandlersId();
      if (Boolean(this.on[parentHdId])){
        this.onEventHandler(this.awaiting[handlerId]);
      }      
    }
  }
  
  var EventHandler  = function( eventName, handlerName, handlerOwner, handlerData){
    this.eventName = eventName;
    this.handlerName = handlerName;
    this.handlerOwner = handlerOwner;
    this.handlerData = handlerData;
    this.realHandler = $.proxy(handlerOwner[handlerName], handlerOwner);
    this.parentHandlerId = null;
    this.childHandlers = new Array();
    
    this.id =  EventHandler.getId(handlerOwner, handlerName);
    if (handlerData.attachAfter){
      var owner = handlerData.attachAfter.handlerOwner || '_anyHandler_';
      this.parentHandlerId = owner + '.' + handlerData.attachAfter.handlerName;
    }
    
  }
  EventHandler.getId = function(handlerOwner, handlerName){
    return handlerOwner.getId() + '.' + handlerName;
  }
  
  EventHandler.prototype.getId = function (){
    return this.id;
  }
  
  EventHandler.prototype.on = function (){
    $(document).on(this.eventName, null, this.handlerData, this.realHandler);
    
    return this;
  }
  
  EventHandler.prototype.off = function (){
    $(document).off(this.eventName, this.realHandler);
    return this;
  }
  
  EventHandler.prototype.clean = function (){
    this.eventName    = null;
    this.handlerName  = null;
    this.handlerOwner = null;
    this.handlerData  = null;
    this.realHandler  = null;
    return this;
  }
  
  EventHandler.prototype.getParentHandlersId = function (){
    return this.parentHandlerId;
  }
  
  EventHandler.prototype.getChildHandlers = function(){
    return this.childHandlers;
  }
  
  EventHandler.prototype.setChildHandlers = function(childHandlers){
    this.childHandlers = childHandlers;
    return this;
  }
  
  Drupal.UiEventController = new Controller();
  
  Drupal.behaviors.uiEvent = {
    attach: function (context, settings) {
      if (!Boolean(Drupal.ajax.prototype.commands.UiEventCommand )){
        Drupal.ajax.prototype.commands.UiEventCommand = $.proxy(Drupal.UiEventController.commandsHandler, Drupal.UiEventController);
      }
    }
  }
  
  
})(jQuery)

